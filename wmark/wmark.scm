;-----------------------------------------------------------------------
;
;  Filename: wmark.scm
;
;-----------------------------------------------------------------------
;
;  Actual version: 1.0 (27.05.2018) - Works with GIMP 2.10.2
;
;-----------------------------------------------------------------------
;
;  Description:
;
;  The 'Wmark' script (Script-Fu) is used with the free & open source
;  image editor GIMP (https://www.gimp.org/) and will create a water-
;  mark on the actual image. In the configuration section of the script
;  the text, fontname, fontsize, position (and some more settings) can
;  be specified. After installing the script, it will appear in the
;  menu 'Script-Fu' under 'Wmark'. This script will also work with the
;  new GIMP version of 2.10.2. This script is based on the Fu-Script
;  called 'watermark.scm' which was created in 2001 by Doug Reynolds.
;  Later it was modified by Patrick Biegel and renamed to 'wmark.scm'.
;
;-----------------------------------------------------------------------
;
;  Information:
;
;  Script-Fu is a Scheme-based language. A defining description of the
;  algorithmic programming language 'Scheme' can be found under the
;  following link:
;
;      http://schemers.org/Documents/Standards/R5RS/HTML/r5rs.html
;
;-----------------------------------------------------------------------
;
;  License:
;
;  This program is free software and you can redistribute it and/or
;  modify it under the terms of the GNU General Public License as pub-
;  lished by the Free Software Foundation; either version 2 of the
;  License, or (at your option) any later version.
;
;  This program is distributed in the hope that it will be useful,
;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;  GNU General Public License for more details.
;
;  The GNU Public License is available at
;  http://www.gnu.org/copyleft/gpl.html
;
;-----------------------------------------------------------------------
;
(define (wmark image drawable)

  (let*

    ; Save the temporary foreground color.
    ((tempcolor (car (gimp-palette-get-foreground))))

    ; Set the foreground color to white.
    (gimp-palette-set-foreground '(255 255 255))

    (let*

      (

        ; ==============================================================
        ;             C O N F I G U R A T I O N   B E G I N
        ; ==============================================================

        ; Specify the company name.
        (company "PB-Soft")

        ; Specify the author of the image.
        (author "PB")

        ; Specify the fontname for the watermark.
        (fontname "Loma")

        ; Specify the fontsize for the watermark.
        (fontsize 20)

        ; Specify the padding (in pixel) from the border.
        (padding 20)

        ; Specify the position of the watermark.
        ;
        ;   # | Vertical | Horizontal
        ;  --------------------------
        ;   0 |  Center  |   Center
        ;   1 |  Center  |   Left
        ;   2 |  Center  |   Right
        ;   3 |  Bottom  |   Left
        ;   4 |  Bottom  |   Right
        ;   5 |  Bottom  |   Center
        ;   6 |  Top     |   Left
        ;   7 |  Top     |   Right
        ;   8 |  Top     |   Center
        ;
        (wmarkpos 0)

        ; Specify the opacity of the watermark.
        (opacity 5)

        ; Specify the watermark text.
        (wmarktext (string-append "© Copyright " (number->string (+ 1900 (car (time)))) " by " company " / " author))

        ; Specify if the watermark layer and image should be combined.
        ;
        ;  0 = No
        ;  1 = Yes
        ;
        (combine 1)

        ; ==============================================================
        ;               C O N F I G U R A T I O N   E N D
        ; ==============================================================

        ; Calculate the image width.
        (imagewidth (car (gimp-image-width image)))

        ; Calculate the image height.
        (imageheight (car (gimp-image-height image)))

        ; Calculate the vertical position y of the watermark (from the top).
        (vertical
          (cond
            ((= wmarkpos 0) (/ (- imageheight fontsize) 2))
            ((= wmarkpos 1) (/ (- imageheight fontsize) 2))
            ((= wmarkpos 2) (/ (- imageheight fontsize) 2))
            ((= wmarkpos 3) (- (- imageheight fontsize) padding))
            ((= wmarkpos 4) (- (- imageheight fontsize) padding))
            ((= wmarkpos 5) (- (- imageheight fontsize) padding))
            ((= wmarkpos 6) padding)
            ((= wmarkpos 7) padding)
            ((= wmarkpos 8) padding)

          )
        )

        ; Calculate the tect length of the watermark.
        (textlength (car (gimp-text-get-extents-fontname wmarktext fontsize 0 fontname)))

        ; Calculate the horizontal position x of the watermark (from the left).
        (horizontal
          (cond
            ((= wmarkpos 0) (/ (- imagewidth textlength) 2))
            ((= wmarkpos 1) padding)
            ((= wmarkpos 2) (- (- imagewidth textlength) padding))
            ((= wmarkpos 3) padding)
            ((= wmarkpos 4) (- (- imagewidth textlength) padding))
            ((= wmarkpos 5) (/ (- imagewidth textlength) 2))
            ((= wmarkpos 6) padding)
            ((= wmarkpos 7) (- (- imagewidth textlength) padding))
            ((= wmarkpos 8) (/ (- imagewidth textlength) 2))
          )
        )

        ; Create a new layer and insert the watermark text.
        (tlayer (car (gimp-text-fontname image -1 horizontal vertical wmarktext -1 TRUE fontsize 0 fontname)))
      )

      ; Add a shadow effect.
      (plug-in-bump-map 1 image tlayer tlayer 135 45 3 0 0 0 0 1 0 0)

      ; Set the opacity of the watermark layer.
      (gimp-layer-set-opacity tlayer opacity)

      ; Check if the watermark layer and the image should be combined.
      (if (= combine 1)

        ; Combine the original image layer and the watermark layer.
        (gimp-image-flatten image)
      )
    )

    ; Set the foreground color back to the temporary color.
    (gimp-palette-set-foreground tempcolor)
  )

  ; Flush all internal changes to the user interface.
  (gimp-displays-flush)
)

(script-fu-register "wmark"
  "<Image>/Script-Fu/Wmark/Create Wmark"
  "Wmark"
  "Doug Reynolds / Patrick Biegel"
  "Doug Reynolds / Patrick Biegel"
  "27.05.2018"
  "RGB*, GRAY*"
  SF-IMAGE "Image" 0
  SF-DRAWABLE "Drawable" 0
)
