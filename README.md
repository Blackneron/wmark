# Wmark Script - README #
---

### Overview ###

The **Wmark** script (Script-Fu) is used with the [**Free & Open Source Image Editor GIMP**](https://www.gimp.org/) and will create a watermark on the actual image. In the configuration section of the script the text, fontname, fontsize, position and some more settings can be specified. After installing the script, it will appear in the menu **Script-Fu** under **Wmark**. This script is based on the Fu-Script called **watermark.scm** which was created in 2001 by **Doug Reynolds**. Later it was modified by **Patrick Biegel** and renamed to **wmark.scm**.

### Screenshots ###

![Wmark - Script-Fu Menu](development/readme/wmark_1.png "Wmark - Script-Fu Menu")
![Wmark - Image with watermark](development/readme/wmark_2.png "Wmark - Image with watermark")

### Setup ###

* Start the GIMP image editor.
* In the menu **Edit/Preferences/Folders/Scripts** you can check the path to the GIMP script directory.
* Edit the configuration section of the script and specify your favorite settings.
* Copy the file **wmark.scm** into this script directory of your GIMP installation.
* Select **Filters/Script-Fu/Refresh Scripts** to reload the available scripts.
* Open an image in the GIMP image editor.
* Choose **Script-Fu/Wmark/Create Wmark** to insert a watermark to the actual image.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **Wmark** script is licensed under the [**GNU General Public License (GPL)**](https://www.gnu.org/copyleft/gpl.html) which is published on the official site of the [**GNU**](https://www.gnu.org/).
